<?php
/*
 * Copyright (C) 2017 Benjamin Letzel benjamin.letzel@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace B2T\AttributeGroups\Catalog\Block\Product\View;

use \Magento\Catalog\Model\Product;
use \Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Description of AttributeGroups
 *
 * @author shelljockey
 */
class AttributeGroups extends \Magento\Framework\View\Element\Template {

    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;
    protected $eavConfig;

    /**
     *
     * @var \B2T\AttributeGroups\Model\ResourceModel\Attribute
     */
    protected $eavAttribute;

    /**
     *
     * @var \B2T\AttributeGroups\Model\ResourceModel\AttributeGroup 
     */
    protected $eavAttributeGroup;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Framework\Registry $registry, PriceCurrencyInterface $priceCurrency, array $data = [], \Magento\Eav\Model\Config $eavConfig, \B2T\AttributeGroups\Model\ResourceModel\Attribute $eavAttribute, \B2T\AttributeGroups\Model\ResourceModel\AttributeGroup $eavAttributeGroup
    ) {
        // Initiate members
        $this->priceCurrency = $priceCurrency;
        $this->_coreRegistry = $registry;
        $this->eavConfig = $eavConfig;
        $this->eavAttribute = $eavAttribute;
        $this->eavAttributeGroup = $eavAttributeGroup;

        parent::__construct($context, $data);
    }

    /**
     * @return Product
     */
    public function getProduct() {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }
        return $this->_product;
    }

    /**
     * $excludeAttr is optional array of attribute codes to
     * exclude them from additional data array
     *
     * @param array $excludeAttr
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getAdditionalGroupedData(array $excludeAttr = []) {

        $data = [];
        $product = $this->getProduct();
        $attributes = $product->getAttributes();

        foreach ($attributes as $attribute) {
            if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
                $frontendInput = $attribute->getFrontendInput();

                $value = $frontendInput == 'select' &&
                        $attribute->getFrontend()->getValue($product) == 'N/A' ? "" : $attribute->getFrontend()->getValue($product);


                $entityAttribute = $this->eavAttribute->getEntityByAttrIdAndSetId(
                        $attribute->getAttributeId(), $product->getAttributeSetId());
                
                $group = $entityAttribute['attribute_group_id'];

                if (is_string($value)) {
                    if (strlen($value) && $product->hasData($attribute->getAttributeCode())) {
                        if ($attribute->getFrontendInput() == 'price') {
                            $value = "";
                        }

                        $group = 0;
                        if ($tmp = $entityAttribute['attribute_group_id']) {
                            $group = $tmp;
                        }


//                        if (!$product->hasData($attribute->getAttributeCode())) {
//                            $value = __('N/A');
//                        } elseif ((string) $value == '') {
//                            $value = __('No');
//                        } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
//                            $value = $this->priceCurrency->convertAndFormat($value);
//                        }
//                        $uom = $attribute->getUom();
                        $data[$group]['items'][$attribute->getAttributeCode()] = array(
                            'label' => $attribute->getStoreLabel(),
                            'value' => (is_float($value) || is_double($value) ? number_format($value, 2, ",", ".") : $value),
                            'code' => $attribute->getAttributeCode(),
                            'frontend_input_type' => $attribute->getFrontendInput()
                        );

                        $data[$group]['attrid'] = $attribute->getAttributeId();
                    }
                }
            }
        }
        foreach ($data AS $groupId => &$group) {
//            var_dump($groupId);
//            var_dump($group);
            $groupArray = $this->eavAttributeGroup->getAttributeGroupById($groupId);
            
//            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//            
//            $groupModel = $objectManager->create('Custom\Module\Model\ResourceModel\Messages\Collection');
//            $group['title'] = $groupModel->getAttributeGroupName();
            $group['title'] = $groupArray['attribute_group_name'];
        }
        return $data;
    }

    /**
     * Receives an entity of eav_entity_attribute table 
     * 
     * @param string $code 
     */
    protected function getEntityAttributeByAttribute($attribute) {

        $entityAttributeId = $this->eavAttribute->getIdByCode(
                $attribute->getEntityType()->getEntityTypeCode(), $attribute->getAttributeCode());

        $entityAttribute = $this->eavAttribute->getEntityAttribute(
                $entityAttributeId);

        return $entityAttribute;
    }

}
