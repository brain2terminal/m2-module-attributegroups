<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace B2T\AttributeGroups\Catalog\Block\Product\View;

use \Magento\Catalog\Model\Product;
use \Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Description of AttributeGroups
 *
 * @author shelljockey
 */
class AttributeGroups extends \Magento\Framework\View\Element\Template {

    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;
    protected $eavConfig;

    /**
     *
     * @var \B2T\AttributeGroups\Model\ResourceModel\Attribute
     */
    protected $eavAttribute;

    /**
     *
     * @var \B2T\AttributeGroups\Model\ResourceModel\AttributeGroup 
     */
    protected $eavAttributeGroup;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\View\Element\Template\Context $context, \Magento\Framework\Registry $registry, PriceCurrencyInterface $priceCurrency, array $data = [], \Magento\Eav\Model\Config $eavConfig, \B2T\AttributeGroups\Model\ResourceModel\Attribute $eavAttribute, \B2T\AttributeGroups\Model\ResourceModel\AttributeGroup $eavAttributeGroup
    ) {
        // Initiate members
        $this->priceCurrency = $priceCurrency;
        $this->_coreRegistry = $registry;
        $this->eavConfig = $eavConfig;
        $this->eavAttribute = $eavAttribute;
        $this->eavAttributeGroup = $eavAttributeGroup;

        parent::__construct($context, $data);
    }

    /**
     * @return Product
     */
    public function getProduct() {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }
        return $this->_product;
    }

    /**
     * $excludeAttr is optional array of attribute codes to
     * exclude them from additional data array
     *
     * @param array $excludeAttr
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function getAdditionalGroupedData(array $excludeAttr = []) {

        $data = [];
        $product = $this->getProduct();
        $attributes = $product->getAttributes();

        foreach ($attributes as $attribute) {
            
//            $id = $attribute->getAttributeId();
//            $model =$attribute->getAttributeModel();
//            
//            var_dump($model);
//            $attribute = $product->getResource()->getAttribute($attribute);
//            echo '///'.$attribute->getData('attribute_group_id').'///';
//            
//            echo $id. ' ';
//            $attribute = $objectManager->create('\Magento\Eav\Model\Attribute');
//            $attribute->load($id);
//            var_dump($attribute);
//            print_r($attribute);


            if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {

                $frontendInput = $attribute->getFrontendInput();

                $value = $frontendInput == 'select' &&
                        $attribute->getFrontend()->getValue($product) == __('N/A') ? "" : $attribute->getFrontend()->getValue($product);


                $entityAttribute = $this->eavAttribute->getEntityByAttrIdAndSetId(
                        $attribute->getAttributeId(), $product->getAttributeSetId());
                $group = $entityAttribute['attribute_group_id'];

//                echo 'Attribute-ID: ' . $attribute->getAttributeId();
//                echo '<br>';
//                echo 'Attribute-Code: ' . $attribute->getAttributeCode();
//                echo '<br>';
//                echo 'Attribute-Set: ' . $product->getAttributeSetId();
//                echo '<br>';
//                echo 'Attribute-Group: ' . $group;

//                var_dump($entityAttribute);
//                var_dump($this->eavAttribute->getEntityByAttrIdAndSetId(
//                                $attribute->getAttributeId(), $product->getAttributeSetId()));
//
//                var_dump($this->eavAttributeGroup->getAttributeGroupById($group));

//                var_dump($entity->getExtensionAttributes());
//                var_dump($attribute->getExtensionAttributes());
//                var_dump($this->getAttributeGroupId($attribute->getAttributeSetId()));

                if (is_string($value)) {
                    if (strlen($value) && $product->hasData($attribute->getAttributeCode())) {
                        if ($attribute->getFrontendInput() == 'price') {
                            $value = "";
                        } elseif (!$attribute->getIsHtmlAllowedOnFront()) {
                            $value = $block->htmlEscape($value);
                        }

                        $group = 0;
                        if ($tmp = $entityAttribute['attribute_group_id']) {
                            $group = $tmp;
                        }


//                        if (!$product->hasData($attribute->getAttributeCode())) {
//                            $value = __('N/A');
//                        } elseif ((string) $value == '') {
//                            $value = __('No');
//                        } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
//                            $value = $this->priceCurrency->convertAndFormat($value);
//                        }
//                        $uom = $attribute->getUom();
                        $data[$group]['items'][$attribute->getAttributeCode()] = array(
                            'label' => __("A"),
                            'value' => (is_float($value) || is_double($value) ? number_format($value, 2, ",", ".") : $value),
                            'code' => $attribute->getAttributeCode(),
                            'frontend_input_type' => $attribute->getFrontendInput()
                        );

                        $data[$group]['attrid'] = $attribute->getAttributeId();
                    }
                }
            }
        }
        foreach ($data AS $groupId => &$group) {
            $groupArray = $this->eavAttributeGroup->getAttributeGroupById($group);
            
//            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//            
//            $groupModel = $objectManager->create('Custom\Module\Model\ResourceModel\Messages\Collection');
//            $group['title'] = $groupModel->getAttributeGroupName();
            $group['title'] = $groupArray['attribute_group_name'];
        }
        return $data;
    }

    /**
     * Receives an entity of eav_entity_attribute table 
     * 
     * @param string $code 
     */
    protected function getEntityAttributeByAttribute($attribute) {

        $entityAttributeId = $this->eavAttribute->getIdByCode(
                $attribute->getEntityType()->getEntityTypeCode(), $attribute->getAttributeCode());

        $entityAttribute = $this->eavAttribute->getEntityAttribute(
                $entityAttributeId);

        return $entityAttribute;
    }

}
