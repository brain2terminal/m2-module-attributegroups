<?php
/*
 * Copyright (C) 2017 Benjamin Letzel benjamin.letzel@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of AttrGroupInterface
 *
 * @author shelljockey
 */
namespace B2T\AttributeGroups\Api\Data;
use Magento\Framework\Api\ExtensibleDataInterface;

interface AttrGroupInterface extends ExtensibleDataInterface
{
    const ATTRIBUTE_ID = "attribute_id";
    const ATTRIBUTE_GROUP_ID= "attribute_group_id"; 
    
    public function getAttributeId();
    public function getAttributeGroupId();
    public function getExtensionAttributes();
    public function setExtensionAttributes
    (
        \B2T\AttributeGroups\Api\Data\AttrGroupExtensionInterface $extensionAttributes
    ); 
}