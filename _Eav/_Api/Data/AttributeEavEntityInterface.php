<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace B2T\AttributeGroups\Api\Data;

/**
 * Description of AttributeEavEntityInterface
 *
 * @author shelljockey
 */
interface AttributeEavEntityInterface extends \Magento\Framework\Api\CustomAttributesDataInterface
{
    const ATTRIBUTE_ID = 'attribute_id';
    
    const ATTRIBUTE_GROUP_ID = 'attribute_group_id';
    
    const ATTRIBUTE_SET_ID = 'attribute_set_id';
    
    const ENTITY_TYPE_ID = 'entity_type_id';
    
    const ENTITY_ATTRIBUTE_ID = 'entity_attribute_id';
    
    
    /**
     * Retrieve id of the attribute.
     *
     * @return int|null
     */
    public function getAttributeId();

  
}

