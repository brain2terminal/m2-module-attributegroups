<?php
/*
 * Copyright (C) 2017 Benjamin Letzel benjamin.letzel@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace B2T\AttributeGroups\Model\ResourceModel;

/**
 * Description of Attribute
 *
 * @author shelljockey
 */
class Attribute extends \Magento\Eav\Model\ResourceModel\Entity\Attribute{
    public function getEntityByAttrIdAndSetId($attributeId, $setId)
    {
        $connection = $this->getConnection();
        $bind = [
                ':attribute_set_id' => $setId,
                ':attribute_id' => $attributeId
                ];
        $select = $connection->select()->from(
            $this->getTable('eav_entity_attribute')
        )->where(
            'attribute_id = :attribute_id'
        )->where(
            'attribute_set_id = :attribute_set_id'
        );

        return $connection->fetchRow($select, $bind);
    }
}
