<?php
/*
 * Copyright (C) 2017 Benjamin Letzel benjamin.letzel@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace B2T\AttributeGroups\Model\ResourceModel;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group as GroupEntity;

/**
 * Description of AttributeGroups
 *
 * @author shelljockey
 */
class AttributeGroup extends GroupEntity {

    public function getAttributeGroupById($groupId) {

        $connection = $this->getConnection();
        $bind = [
            'attribute_group_id' => $groupId
        ];
        $select = $connection->select()->from(
                        $this->getMainTable()
                )->where(
                'attribute_group_id = :attribute_group_id'
        );

        return $connection->fetchRow($select, $bind);
    }

}
