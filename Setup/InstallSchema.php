<?php
/*
 * Copyright (C) 2017 Benjamin Letzel benjamin.letzel@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of InstallSchema
 *
 * @author Benjamin Letzel <support@brain2terminal.com>
 */

namespace B2T\ExpertsReport\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface {

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()
                ->newTable($installer->getTable('b2t_attribute_group'))
                ->addColumn('id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                        ), 'ID')
                ->addColumn(
                        'attribute_group_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                    'nullable' => false,
                    'unsigned' => true
                        ), 'Attribute Group ID')
                ->addColumn(
                        'attribute_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                    'nullable' => false,
                    'unsigned' => true
                        ), 'Attribute ID');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }

}
